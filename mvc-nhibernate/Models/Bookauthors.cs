using System;
using System.Text;
using System.Collections.Generic;


namespace mvc_nhibernate.Models {
    
    public class BookAuthors {
        public virtual int Bookid { get; set; }
        public virtual Book Book { get; set; }
        public virtual Author Author { get; set; }
    }
}
