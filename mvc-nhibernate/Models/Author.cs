using System;
using System.Text;
using System.Collections.Generic;


namespace mvc_nhibernate.Models {
    
    public class Author {
        public Author() { }
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
    }
}
