using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using mvc_nhibernate.Models; 

namespace mvc_nhibernate.Models {
    
    
    public class AuthorsMap : ClassMap<Author> {
        
        public AuthorsMap() {
			Table("Authors");
			LazyLoad();
			Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			Map(x => x.Name).Column("Name");
        }
    }
}
