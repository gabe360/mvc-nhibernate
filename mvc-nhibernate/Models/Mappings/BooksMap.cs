using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using mvc_nhibernate.Models; 

namespace mvc_nhibernate.Models {
    
    
    public class BooksMap : ClassMap<Book> {
        
        public BooksMap() {
			Table("Books");
			LazyLoad();
			Id(x => x.Id).GeneratedBy.Identity().Column("Id");
			Map(x => x.Isbn).Column("Isbn");
			Map(x => x.Title).Column("Title");
			Map(x => x.Publisher).Column("Publisher");
			Map(x => x.PublishedDate).Column("PublishedDate");
        }
    }
}
