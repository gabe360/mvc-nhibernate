using System; 
using System.Collections.Generic; 
using System.Text; 
using FluentNHibernate.Mapping;
using mvc_nhibernate.Models; 

namespace mvc_nhibernate.Models {
    
    
    public class BookauthorsMap : ClassMap<BookAuthors> {
        
        public BookauthorsMap() {
			Table("BookAuthors");
			LazyLoad();
			Id(x => x.Bookid).GeneratedBy.Identity().Column("BookId");
			References(x => x.Book).Column("BookId");
			References(x => x.Author).Column("AuthorId");
        }
    }
}
