using System;
using System.Text;
using System.Collections.Generic;


namespace mvc_nhibernate.Models {
    
    public class Book {
        public Book() { }
        public virtual int Id { get; set; }
        public virtual string Isbn { get; set; }
        public virtual string Title { get; set; }
        public virtual string Publisher { get; set; }
        public virtual string PublishedDate { get; set; }
    }
}
