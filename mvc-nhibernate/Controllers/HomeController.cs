﻿using mvc_nhibernate.Models;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace mvc_nhibernate.Controllers
{
    public class HomeController : Controller
    {
        private readonly ISession _session;

        public HomeController(ISession session)
        {
            _session = session;
        }

        public ActionResult Index()
        {
            //Book book = new Book { Isbn = "1234567890", PublishedDate = "2013", Publisher = "McGraw Hill", Title = "The Guide to World Domination" };
            //_session.Save(book);
            
            var books = _session.QueryOver<Book>()
                .OrderBy(b => b.Title).Asc
                .List();

            return View(books);
        }
    }
}
